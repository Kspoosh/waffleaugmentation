﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Speech.Synthesis;
using System.Windows.Forms;
using System.Collections;
using System.Collections.Specialized;


namespace TTStest
{
    public partial class main : Form
    {
        string history_one = "";
        int devicenumber = Properties.Settings.Default.Device;            //-1 = default playback device | 0 = first device and so on
        bool isStartup = true;
        int usedTagsList = 0;
        int tagAmount = 0;
        List<Button> buttonsadded = new List<Button>();

        List<Point> availableLocations = new List<Point>();
        List<Point> usedLocations = new List<Point>();
        List<int> availableTags = new List<int>();
        List<int> usedTags = new List<int>();
        List<String> buttonTexts = new List<string>();
        StringCollection btnText = new StringCollection();
        StringCollection btnTextTemp = new StringCollection();
        List<int> usedTagsTemp = new List<int>();
        List<Point> usedLocationsTemp = new List<Point>();        

        private WaveFileReader wavRead = null;
        private WaveOutEvent waveOutput = null;
        private RawSourceWaveStream waveStream = null;
        private MemoryStream stream = null;
        private SpeechSynthesizer synth = null;
        
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        public main()
        {
            tagAmount = usedTags.Count();
            InitializeComponent();
            initCombobox();
            this.KeyPreview = true;
            buttonTexts = Properties.Settings.Default.buttonTexts;
            try
            {
                usedTagsList = Properties.Settings.Default.usedTags.Count();
            }
            catch(System.ArgumentNullException)
            { usedTagsList = 0; }
            if (Properties.Settings.Default.firstStart == true)
            { 
                for (int locs = 68, y = 1; locs < 360; locs = locs + 29, y++)
                {
                    availableLocations.Add(new Point(346, locs));
                    availableTags.Add(y);
                }
                Properties.Settings.Default.availableLocations = availableLocations;
                Properties.Settings.Default.availableTags = availableTags;
                Properties.Settings.Default.firstStart = false;
                Properties.Settings.Default.Save();
            }
            else if(isStartup == true && usedTagsList != 0)
            {
                availableLocations = Properties.Settings.Default.availableLocations;
                usedLocations = Properties.Settings.Default.usedLocations;
                availableTags = Properties.Settings.Default.availableTags;
                usedTags = Properties.Settings.Default.usedTags;
                List<int> tagList = new List<int>();
                tagList = Properties.Settings.Default.usedTags;
                tagAmount = usedTags.Count();
                while (tagAmount > 0)
                {
                    add_Button();
                    tagAmount--;
                }
                List<String> buttonTexts = btnTextTemp.Cast<String>().ToList();
                usedTags = usedTagsTemp;
                usedLocations = usedLocationsTemp;
                isStartup = false;                
            }
            else { isStartup = false; }
            isStartup = false;
        }

        public void initCombobox()
        {
            //populate the comobobox with all installed TTS Voices
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
            using (SpeechSynthesizer synth = new SpeechSynthesizer())
            {
                 foreach (InstalledVoice voice in synth.GetInstalledVoices())
                 {
                    VoiceInfo info = voice.VoiceInfo;
                    comboBox1.Items.Add(info.Name);
                 }
                comboBox1.SelectedIndex = Properties.Settings.Default.selectedVoice;
            }
        }

        public void TTStest(string tospeak)
        {
            stream = new MemoryStream();
            synth = new SpeechSynthesizer();
            synth.SelectVoice(comboBox1.SelectedItem.ToString());                                       //gets the voice out of the combobox
            synth.SetOutputToWaveStream(stream);                                                        //sets the System.Speech output to a WaveStream
            synth.Speak(tospeak);                                                                       //Speaks the string in tospeak
            player(stream);                                                                             //calls the player and passes the WaveStream eventhough it's a MemoryStream stupid programming
            if (listBox1.Items.Count == 26)                                                             //handles the History listbox, removes old entries
            {
                listBox1.Items.RemoveAt(0);
            }
            if(richTextBox1.Text != "" && richTextBox1.Text == tospeak)                                 //makes sure that only text from the richtextbox gets added to the history
            { 
                history_one = richTextBox1.Text;                                                        //add text to history_one ie. press up arrow to get the last read text
                listBox1.Items.Add(richTextBox1.Text);                                                  //add the text to the history list
            }
            richTextBox1.Enabled = false;                                                               //deactivate richtextbox while the program clears everything
        }

        public void player(MemoryStream stream)
        {
            waveStream = new RawSourceWaveStream(stream, new WaveFormat(16000, 8, 1));                  //Create a usable WaveStream out of the memoryStream stream passed from the TTStest method and set the format to 16000 bits/s, 8bit, mono
            waveStream.Position = 0;                                                                    //"Rewind" the WaveStream, thats right just like in the old days with tapes and stuff
            wavRead = new WaveFileReader(waveStream);                                                   //read the WaveStream so that it can be used by the waveOutput
            waveOutput = new WaveOutEvent();                                                            //Create the player
            //waveOutput.DesiredLatency = wavRead.TotalTime.Milliseconds;
            waveOutput.DeviceNumber = devicenumber;                                                     //set the output device
            waveOutput.Init(wavRead);                                                                   //initialize the player with the wavestream to play
            waveOutput.PlaybackStopped += new EventHandler<StoppedEventArgs>(CleanUp);                  //add an eventhandler to detect the playback.stopped() event then call CleanUp
            waveOutput.Play();                                                                          //Finally, play the stream
        }

        void CleanUp(object sender, StoppedEventArgs e)                                                                        //Get rid of all the stuff
        {
            if (richTextBox1.Text != null || richTextBox1.Text != "")
            {
                richTextBox1.Clear();
            }

            if (wavRead != null)
            {
                wavRead.Dispose();
                wavRead = null;
            }
            if (waveOutput != null)
            {
                waveOutput.Dispose();
                waveOutput.Stop();
                waveOutput = null;
            }
            if (waveStream != null)
            {
                waveStream.Dispose();
                waveStream.Flush();
                waveStream = null;
            }
            if (stream != null)
            {
                stream.Dispose();
                stream.Flush();
                stream = null;
            }
            if (synth != null)
            {
                synth.Dispose();
                synth = null;
            }
            richTextBox1.Enabled = true;
            richTextBox1.Focus();
        }

        private void ontop_CheckedChanged(object sender, EventArgs e)                                                          //makes the window stay on top
        {
            if (ontop.Checked == true)
            {
                this.TopMost = true;
            }
            else
            {
                this.TopMost = false;
            }

        }

        private void trackBar1_Scroll(object sender, EventArgs e)                                                              //change the window opacity with the trackbar
        {
            string trackval = Convert.ToString(((double)trackBar1.Value) / 100);
            Opacity = ((double)trackBar1.Value) / 100;
        }

        private void comboBox1_DrawItem(object sender, DrawItemEventArgs e)                                                    //Let's not talk about that
        {
           if (e.Index == -1)
           {
               return;
           }
           Point p = new Point(comboBox1.Location.X + 120, comboBox1.Location.Y + comboBox1.Height + (30 + e.Index * 10));
           if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
           {
               //toolTip1.Show(comboBox1.Items[e.Index].ToString(), this, p);
           }
           e.DrawBackground();
           e.Graphics.DrawString(comboBox1.Items[e.Index].ToString(), e.Font, Brushes.Black, new Point(e.Bounds.X, e.Bounds.Y));
        }

        private void main_KeyDown(object sender, KeyEventArgs e)                                                               //Shortcuts are always nice
        {

            if (e.KeyCode == Keys.Return && richTextBox1.Text != "")
            {
                TTStest(richTextBox1.Text);

            }
            else if (e.KeyCode == Keys.Up)
            {
                richTextBox1.Text = history_one;
            }
            else { }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)                                                 //Click in the History playback that entry
        {
            try
            {
                TTStest(listBox1.SelectedItem.ToString());
            }
            catch (NullReferenceException)
            {
            }
        }

        private void add_Button()                                                                                              //Check if it's the startup routine if yes then add the buttons that are saved in the usersettings, if no the just normally add a button
        {
            if(isStartup == true && tagAmount != 0)
            {
                int buttontextcount = buttonTexts.Count();
                string buttonresult = buttonTexts.First();
                btnTextTemp.Add(buttonresult);
                buttonTexts.Remove(buttonresult);

                Button button = new Button();

                int minTag = usedTags.First();
                usedTags.Remove(minTag);
                usedTagsTemp.Add(minTag);
                button.Tag = minTag;

                Point minpoint = usedLocations.First();
                usedLocations.Remove(minpoint);
                usedLocationsTemp.Add(minpoint);
                button.Location = minpoint;

                toolTip1.SetToolTip(button, "Shift + Left Click to delete");
                button.Width = 132;

                button.Click += new EventHandler(ButtonClickOneEvent);
                button.Text = buttonresult;
                Controls.Add(button);
                buttonsadded.Insert(0, button);
                linklabelVisbility();                
            }
            else
            {
                Input testDialog = new Input();

                // Show testDialog as a modal dialog and determine if DialogResult = OK.
                if (testDialog.ShowDialog(this) == DialogResult.OK)
                {
                    int tagslist = Properties.Settings.Default.availableTags.Count();
                    int minTag = Properties.Settings.Default.availableTags.Min();
                    availableLocations = Properties.Settings.Default.availableLocations;
                    string txtResult;
                    if (tagslist == 0)
                    { return; }
                    txtResult = testDialog.textBox1.Text;
                    btnTextTemp.Add(txtResult);
                    Button button = new Button();
                    button.Tag = minTag;
                    availableTags.Remove(minTag);
                    usedTags.Add(minTag);
                    btnText.Add(txtResult);
                    Point minpoint = availableLocations.First(p => p.Y == availableLocations.Min(po => po.Y));
                    availableLocations.Remove(minpoint);
                    usedLocations.Add(minpoint);
                    button.Location = minpoint;
                    toolTip1.SetToolTip(button, "Shift + Left Click to delete");
                    button.Width = 132;
                    button.Click += new EventHandler(ButtonClickOneEvent);
                    button.Text = txtResult;
                    Controls.Add(button);
                    buttonsadded.Insert(0, button);
                    linklabelVisbility();
                    List<String> buttonTexts = btnText.Cast<String>().ToList();
                    Properties.Settings.Default.buttonTexts = buttonTexts;
                    Properties.Settings.Default.availableLocations = availableLocations;
                    Properties.Settings.Default.usedLocations = usedLocations;
                    Properties.Settings.Default.availableTags = availableTags;
                    Properties.Settings.Default.usedTags = usedTags;
                    Properties.Settings.Default.Save();
                }
                else
                { }
                testDialog.Dispose();
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)                                     //Button Creator
        {
            add_Button();
        }

        private void linklabelVisbility()
        {
            if (availableTags.Count < 1)
            { linkLabel1.Visible = false; }
            else { linkLabel1.Visible = true; }
        }

        void ButtonClickOneEvent(object sender, EventArgs e)                                                                    //eventhandler for new buttons
        {
            Button button = sender as Button;
            if (button != null)
            {
                switch ((int)button.Tag)
                {
                    case 1:
                        if (Control.ModifierKeys == Keys.Shift)
                        {
                            Button buttonToRemove = (this.GetChildAtPoint(this.PointToClient(Cursor.Position)) as Button);
                            if (buttonsadded.Contains(buttonToRemove))
                            {
                                buttonsadded.Remove(buttonToRemove);
                                usedLocations.Remove(buttonToRemove.Location);
                                availableLocations.Add(buttonToRemove.Location);
                                availableTags.Add(Convert.ToInt32(buttonToRemove.Tag));
                                usedTags.Remove(Convert.ToInt32(buttonToRemove.Tag));
                                Controls.Remove(buttonToRemove);
                                linklabelVisbility();
                            }                            
                        }
                        else
                        {
                            TTStest(button.Text);
                        }
                        break;
                    case 2:
                        if (Control.ModifierKeys == Keys.Shift)
                        {
                            Button buttonToRemove = (this.GetChildAtPoint(this.PointToClient(Cursor.Position)) as Button);
                            if (buttonsadded.Contains(buttonToRemove))
                            {
                                buttonsadded.Remove(buttonToRemove);
                                usedLocations.Remove(buttonToRemove.Location);
                                availableLocations.Add(buttonToRemove.Location);
                                availableTags.Add(Convert.ToInt32(buttonToRemove.Tag));
                                usedTags.Remove(Convert.ToInt32(buttonToRemove.Tag));
                                Controls.Remove(buttonToRemove);
                                linklabelVisbility();
                            }
                        }
                        else
                        {
                            TTStest(button.Text);
                        }
                        break;
                    case 3:
                        if (Control.ModifierKeys == Keys.Shift)
                        {
                            Button buttonToRemove = (this.GetChildAtPoint(this.PointToClient(Cursor.Position)) as Button);
                            if (buttonsadded.Contains(buttonToRemove))
                            {
                                buttonsadded.Remove(buttonToRemove);
                                usedLocations.Remove(buttonToRemove.Location);
                                availableLocations.Add(buttonToRemove.Location);
                                availableTags.Add(Convert.ToInt32(buttonToRemove.Tag));
                                usedTags.Remove(Convert.ToInt32(buttonToRemove.Tag));
                                Controls.Remove(buttonToRemove);
                                linklabelVisbility();
                            }
                        }
                        else
                        {
                            TTStest(button.Text);
                        }
                        break;
                    case 4:
                        if (Control.ModifierKeys == Keys.Shift)
                        {
                            Button buttonToRemove = (this.GetChildAtPoint(this.PointToClient(Cursor.Position)) as Button);
                            if (buttonsadded.Contains(buttonToRemove))
                            {
                                buttonsadded.Remove(buttonToRemove);
                                usedLocations.Remove(buttonToRemove.Location);
                                availableLocations.Add(buttonToRemove.Location);
                                availableTags.Add(Convert.ToInt32(buttonToRemove.Tag));
                                usedTags.Remove(Convert.ToInt32(buttonToRemove.Tag));
                                Controls.Remove(buttonToRemove);
                                linklabelVisbility();
                            }
                        }
                        else
                        {
                            TTStest(button.Text);
                        }
                        break;
                    case 5:
                        if (Control.ModifierKeys == Keys.Shift)
                        {
                            Button buttonToRemove = (this.GetChildAtPoint(this.PointToClient(Cursor.Position)) as Button);
                            if (buttonsadded.Contains(buttonToRemove))
                            {
                                buttonsadded.Remove(buttonToRemove);
                                usedLocations.Remove(buttonToRemove.Location);
                                availableLocations.Add(buttonToRemove.Location);
                                availableTags.Add(Convert.ToInt32(buttonToRemove.Tag));
                                usedTags.Remove(Convert.ToInt32(buttonToRemove.Tag));
                                Controls.Remove(buttonToRemove);
                                linklabelVisbility();
                            }
                        }
                        else
                        {
                            TTStest(button.Text);
                        }
                        break;
                    case 6:
                        if (Control.ModifierKeys == Keys.Shift)
                        {
                            Button buttonToRemove = (this.GetChildAtPoint(this.PointToClient(Cursor.Position)) as Button);
                            if (buttonsadded.Contains(buttonToRemove))
                            {
                                buttonsadded.Remove(buttonToRemove);
                                usedLocations.Remove(buttonToRemove.Location);
                                availableLocations.Add(buttonToRemove.Location);
                                availableTags.Add(Convert.ToInt32(buttonToRemove.Tag));
                                usedTags.Remove(Convert.ToInt32(buttonToRemove.Tag));
                                Controls.Remove(buttonToRemove);
                                linklabelVisbility();
                            }
                        }
                        else
                        {
                            TTStest(button.Text);
                        }
                        break;
                    case 7:
                        if (Control.ModifierKeys == Keys.Shift)
                        {
                            Button buttonToRemove = (this.GetChildAtPoint(this.PointToClient(Cursor.Position)) as Button);
                            if (buttonsadded.Contains(buttonToRemove))
                            {
                                buttonsadded.Remove(buttonToRemove);
                                usedLocations.Remove(buttonToRemove.Location);
                                availableLocations.Add(buttonToRemove.Location);
                                availableTags.Add(Convert.ToInt32(buttonToRemove.Tag));
                                usedTags.Remove(Convert.ToInt32(buttonToRemove.Tag));
                                Controls.Remove(buttonToRemove);
                                linklabelVisbility();
                            }
                        }
                        else
                        {
                            TTStest(button.Text);
                        }
                        break;
                    case 8:
                        if (Control.ModifierKeys == Keys.Shift)
                        {
                            Button buttonToRemove = (this.GetChildAtPoint(this.PointToClient(Cursor.Position)) as Button);
                            if (buttonsadded.Contains(buttonToRemove))
                            {
                                buttonsadded.Remove(buttonToRemove);
                                usedLocations.Remove(buttonToRemove.Location);
                                availableLocations.Add(buttonToRemove.Location);
                                availableTags.Add(Convert.ToInt32(buttonToRemove.Tag));
                                usedTags.Remove(Convert.ToInt32(buttonToRemove.Tag));
                                Controls.Remove(buttonToRemove);
                                linklabelVisbility();
                            }
                        }
                        else
                        {
                            TTStest(button.Text);
                        }
                        break;
                    case 9:
                        if (Control.ModifierKeys == Keys.Shift)
                        {
                            Button buttonToRemove = (this.GetChildAtPoint(this.PointToClient(Cursor.Position)) as Button);
                            if (buttonsadded.Contains(buttonToRemove))
                            {
                                buttonsadded.Remove(buttonToRemove);
                                usedLocations.Remove(buttonToRemove.Location);
                                availableLocations.Add(buttonToRemove.Location);
                                availableTags.Add(Convert.ToInt32(buttonToRemove.Tag));
                                usedTags.Remove(Convert.ToInt32(buttonToRemove.Tag));
                                Controls.Remove(buttonToRemove);
                                linklabelVisbility();
                            }
                        }
                        else
                        {
                            TTStest(button.Text);
                        }
                        break;
                    case 10:
                        if (Control.ModifierKeys == Keys.Shift)
                        {
                            Button buttonToRemove = (this.GetChildAtPoint(this.PointToClient(Cursor.Position)) as Button);
                            if (buttonsadded.Contains(buttonToRemove))
                            {
                                buttonsadded.Remove(buttonToRemove);
                                usedLocations.Remove(buttonToRemove.Location);
                                availableLocations.Add(buttonToRemove.Location);
                                availableTags.Add(Convert.ToInt32(buttonToRemove.Tag));
                                usedTags.Remove(Convert.ToInt32(buttonToRemove.Tag));
                                Controls.Remove(buttonToRemove);
                                linklabelVisbility();
                            }
                        }
                        else
                        {
                            TTStest(button.Text);
                        }
                        break;
                    case 11:
                        if (Control.ModifierKeys == Keys.Shift)
                        {
                            Button buttonToRemove = (this.GetChildAtPoint(this.PointToClient(Cursor.Position)) as Button);
                            if (buttonsadded.Contains(buttonToRemove))
                            {
                                buttonsadded.Remove(buttonToRemove);
                                usedLocations.Remove(buttonToRemove.Location);
                                availableLocations.Add(buttonToRemove.Location);
                                availableTags.Add(Convert.ToInt32(buttonToRemove.Tag));
                                usedTags.Remove(Convert.ToInt32(buttonToRemove.Tag));
                                Controls.Remove(buttonToRemove);
                                linklabelVisbility();
                            }
                        }
                        else
                        {
                            TTStest(button.Text);
                        }
                        break;
                }
            }
        }

        private void common_Hello_Click(object sender, EventArgs e)                                                             //default common phrase button
        {
            TTStest(common_Hello.Text);
        }

        private void hide_controls(object sender, EventArgs e)                                                                  //here we're handling what happens if you double right click in the windo
        {
            if (this.FormBorderStyle == FormBorderStyle.FixedSingle)                                                                //Make it small
            {
                this.FormBorderStyle = FormBorderStyle.None;
                this.Size = new Size(346, 100);
                richTextBox1.Multiline = false;
                richTextBox1.Size = new Size(328, 23);
                this.CenterToScreen();
                trackBar1.Visible = false;
                ontop.Location = new Point(12, 65);
            }
            else                                                                                                                //Make it big again
            {
                this.FormBorderStyle = FormBorderStyle.FixedSingle;
                this.Size = new Size(659, 492);
                this.CenterToScreen();
                richTextBox1.Multiline = true;
                trackBar1.Visible = true;
                richTextBox1.Size = new Size(328, 342);
                ontop.Location = new Point(12, 387);
            }
        }

        private void soundSettings_btn_Click(object sender, EventArgs e)                                                        //opens up Soundsetup.cs and shows it as a dialog
        {
            Soundsetup soundDialog = new Soundsetup();

            if (soundDialog.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
                try
                {
                    devicenumber = Properties.Settings.Default.Device;
                }
                catch (ArgumentOutOfRangeException){}
            }
            else{}
            soundDialog.Dispose();                                                                                              //yay, more to get rid of  

        }

        private void main_FormClosing(object sender, FormClosingEventArgs e)                                                    // add settings that should be saved
        {
            Properties.Settings.Default.selectedVoice = comboBox1.SelectedIndex;
            Properties.Settings.Default.availableLocations = availableLocations;
            Properties.Settings.Default.usedLocations = usedLocations;
            Properties.Settings.Default.availableTags = availableTags;
            Properties.Settings.Default.usedTags = usedTags;
            List<String> buttonTexts = btnTextTemp.Cast<String>().ToList();
            Properties.Settings.Default.buttonTexts = buttonTexts;

            Properties.Settings.Default.Save();
        }
    }
}