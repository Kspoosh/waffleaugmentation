﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NAudio;
using NAudio.Wave;

namespace TTStest
{
    public partial class Soundsetup : Form
    {
      
        public void Devicess()
        {
            List<WaveOutCapabilities> output = new List<WaveOutCapabilities>();

            for (int i = 0; i < WaveOut.DeviceCount; i++)
            {
                output.Add(WaveOut.GetCapabilities(i));
            }

            listView1.Items.Clear();

            foreach (var device in output)
            {
                ListViewItem item = new ListViewItem(device.ProductName);
                listView1.Items.Add(item);
            }

            List<WaveInCapabilities> input = new List<WaveInCapabilities>();

            for (int i=0; i < WaveIn.DeviceCount; i++)
            {
                input.Add(WaveIn.GetCapabilities(i));
            }

            sourceList.Items.Clear();

            foreach (var source in input)
            {
                ListViewItem sources = new ListViewItem(source.ProductName);
                sourceList.Items.Add(sources);
            }
        }

        public Soundsetup()
        {
            InitializeComponent();
            Devicess();
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Properties.Settings.Default.Device = listView1.SelectedItems[0].Index;
            Properties.Settings.Default.Save();
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void main_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                DialogResult = DialogResult.Cancel;
            }
        }
    }
}
